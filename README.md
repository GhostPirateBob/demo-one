# demo-one

## Demo Link

[https://s3.ap-southeast-2.amazonaws.com/ghostpirates/demos/demo-one/index.html](https://s3.ap-southeast-2.amazonaws.com/ghostpirates/demos/demo-one/index.html)

One page template, a great choice for creating a personal portfolio website, including services, projects and contact form with nice and smooth scrolling through the sections. Modern and clean HTML5 template, perfect for your online CV.
